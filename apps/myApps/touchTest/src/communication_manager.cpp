//
//  communication_manager.cpp
//  robot_interface
//
//  Created by Joao Freire on 11/26/15.
//
//

#include "communication_manager.h"

void communication_manager::create()
{
    	
	//	msgTx = " {\"op\":\"advertise\",\"topic\":\"/chatter\",\"type\":\"std_msgs/String\"}";
	// advanced: set keep-alive timeouts for events like
    // loss of internet
    
    // 1 - get default options
    ofxLibwebsockets::ClientOptions options = ofxLibwebsockets::defaultClientOptions();
    
    // 2 - set basic params
    options.host = "ws://192.168.2.24";
    
    // 3 - set keep alive params
    // BIG GOTCHA: on BSD systems, e.g. Mac OS X, these time params are system-wide
    // ...so ka_time just says "check if alive when you want" instead of "check if
    // alive after X seconds"
    // Note: some have had issues with only setting one of these; may be an 
    // all-or-nothing type option!
    options.ka_time     = 1;
    options.ka_probes   = 1;
    options.ka_interval = 1;
    // 4 - connect
    client.connect("localhost",9090);
    
    ofSetLogLevel(OF_LOG_ERROR);
    
    client.addListener(this);
	client.send(" {\"op\":\"advertise\",\"topic\":\"/idmind_interface/\",\"type\":\"std_msgs/String\"}");
	client.send(" {\"op\":\"subscribe\",\"topic\":\"/idmind_interface/\"}");
    client.send(" {\"op\":\"subscribe\",\"topic\":\"/idmind_touch/position\"}");
		
}

void communication_manager::update()
{
    
}

void communication_manager::send(string msg)
{
    string send;
    send = "{ \"op\": \"publish\", \"topic\": \"/idmind_interface/\",\"msg\": {\"data\":\"action:" + msg+ "\"}}";
    if(msg == "done")
    {    
        client.send(send);
        ofSleepMillis(500);
        send = "{ \"op\": \"publish\", \"topic\": \"/idmind_interface/\",\"msg\": {\"data\":\"action:donothing\"}}";
        client.send(send);
    }
    else
        client.send(send);
    cout<<"SEND MESSAGE "<<send<<endl;
}
bool communication_manager::hasNewMsg()
{
    return _newMsg;
}

string communication_manager::getLastMsg()
{
    _newMsg = false;
    return lastMsg;
}
void communication_manager::draw()
{
    ofSetColor(ofColor::yellow);
    ofDrawCirle(touchPoint, 5);
}

void communication_manager::decodeMsg(string msg)
{
    _newMsg = true;
	msgParser.parse(msg);
	string topic = msgParser["topic"].asString();
	if(topic == "/idmind_interface/")
	{
        lastMsg = msgParser["msg"]["data"].asString();
	}
    else if(topic =="/idmind_touch/position")
    {
        int x = msgParser["msg"]["x"].asInt();
        int y = msgParser["msg"]["y"].asInt();
        cout<<"TOUCH AT "<<x<<" : "<<y<<endl;
        touchPoint.x = 

    }
}
//--------------------------------------------------------------
void communication_manager::onConnect( ofxLibwebsockets::Event& args ){
    cout<<"on connected"<<endl;
}

//--------------------------------------------------------------
void communication_manager::onOpen( ofxLibwebsockets::Event& args ){
    cout<<"on open"<<endl;
        send("connect");

}

//--------------------------------------------------------------
void communication_manager::onClose( ofxLibwebsockets::Event& args ){
    cout<<"on close"<<endl;
}

//--------------------------------------------------------------
void communication_manager::onIdle( ofxLibwebsockets::Event& args ){
    cout<<"on idle"<<endl;
}

//--------------------------------------------------------------
void communication_manager::onMessage( ofxLibwebsockets::Event& args ){
    cout<<"got message "<<args.message<<endl;
	decodeMsg(args.message);
}

//--------------------------------------------------------------
void communication_manager::onBroadcast( ofxLibwebsockets::Event& args ){
    cout<<"got broadcast "<<args.message<<endl;
}
  
