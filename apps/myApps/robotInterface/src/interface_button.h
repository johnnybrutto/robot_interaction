//
//  interface_button.h
//  robot_interface
//
//  Created by Joao Freire on 19/10/15.
//
//

#ifndef interface_button_h
#define interface_button_h

#include <stdio.h>
#pragma once

#include "ofMain.h"
#include "ofxJSON.h"

enum button_type
{
    BUTTON_IMAGE,
    BUTTON_VIDEO,
    BUTTON_ACTION
};

class button
{
public:
    void init(int _iD, string _tag, button_type _type,int size, int y);
    bool isClicked(int x, int y);
    void draw();
    button_type getType();
    void setColor(ofColor _back, ofColor _front);

private:
    string tag;
    int iD;
    ofRectangle buttonArea;
    button_type type;
    int width;
    int height;
    bool _clicked;
    ofTrueTypeFont *verdana;
    ofColor back, front;

};


#endif /* interface_button_h */
