#pragma once

#include "ofMain.h"
#include "ofxJSON.h"
#include "interface_manager.h"
#include "communication_manager.h"
#include "ofxGui.h"
#include "ofxArgs.h"
class ofApp : public ofSimpleApp
{

public:
	ofApp(ofxArgs* args);

	void setup();
	void createGui();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
    void exit();
private:
	ofxArgs* args;
	string option1, option2;
	bool flag1;
    
    interface_manager manager;
    communication_manager COM;
  	int nFingers;
	ofTrueTypeFont verdana;
	//GUI
	ofxPanel gui;
	ofxToggle mouseInput;
	ofxColorSlider backColor, frontColor, buttonColor;
	ofxLabel interaction_topic;
	ofxLabel touch_topic;
	ofxLabel root_path;
	ofxLabel json_path;
bool gui_set;
bool interface_set;

	bool _hideGui;
};
