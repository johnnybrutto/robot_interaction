//
//  interface_manager.h
//  robot_interface
//
//  Created by Joao Freire on 11/20/15.
//
//

#ifndef interface_manager_h
#define interface_manager_h
#pragma once
#include <stdio.h>

#include "ofMain.h"
#include "ofxJSON.h"
#include "interface_point.h"
#include "ofxLibwebsockets.h"
#include <iostream>

class interface_manager
{
public:
    void create(string _json, ofColor buttonButtonColor, ofColor frontButtonColor, ofColor backButtonColor);
    void update();
    void draw();
    void checkTouch(int mouseX, int mouseY);
    void checkTouch(ofPoint touchPoint);
    void changeInterface(int key);
    void receiveCommand(string command);
    bool hasNewMsg();
    void resetNewMsg();
    string getMsg();

private:
    int nPoints;
    map <string, interface_point> interface;
    string activeInterface;
    int activePoint;
    ofImage back_main;
    ofxJSONElement json;
    bool _newMsg;
    string message;
    ofColor backColor;
    ofPoint touchPoint;
};
#endif /* interface_manager_h */
