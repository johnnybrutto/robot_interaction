//
//  communication_manager.h
//  robot_interface
//
//  Created by Joao Freire on 11/26/15.
//
//

#ifndef communication_manager_h
#define communication_manager_h
#pragma once
#include <stdio.h>

#include "ofMain.h"
#include "ofxLibwebsockets.h"
#include "ofxJSON.h"

class communication_manager
{
public:
	void create(string _interaction_topic, string _touch_topic);
	void update();
	void draw();
	bool hasNewMsg();
	bool hasNewTouch();
	string getLastMsg();
	void decodeMsg(string msg);
   	void onConnect( ofxLibwebsockets::Event& args );
    void onOpen( ofxLibwebsockets::Event& args );
    void onClose( ofxLibwebsockets::Event& args );
    void onIdle( ofxLibwebsockets::Event& args );
    void onMessage( ofxLibwebsockets::Event& args );
    void onBroadcast( ofxLibwebsockets::Event& args );
    void send(string msg);
    ofPoint getTouch();
    
private:
    ofxLibwebsockets::Client client;
	string msgTx, msgRx;
	float counter;
	int connectTime;
	int deltaTime;
	bool isConnected;
	int size;
	int pos;
	bool typed;
	ofxJSONElement msgParser;
	bool _newMsg;
	bool _newTouch;
	string lastMsg;
	ofPoint touchPoint;
	string interaction_topic, touch_topic;
};
#endif /* communication_manager_h */
