//
//  interface_manager.cpp
//  robot_interface
//
//  Created by Joao Freire on 11/20/15.
//
//

#include "interface_manager.h"

void interface_manager::create(string _json, ofColor buttonColor, ofColor frontButtonColor, ofColor _backColor)
{
    backColor = _backColor;
    back_main.loadImage("images/viva.png");
    activeInterface = "";
    activePoint = -1;
    std::string file = _json;
    json.open(file);
    
    nPoints = json["Points"].size();
    for(int i = 0; i < nPoints; i++)
    {
        interface_point aux;
        string tag = json["Points"][i]["ID"].asString();
        bool _auxpermanent;
        if(!json["Points"][i]["permanent"].empty())
        {
            _auxpermanent = json["Points"][i]["permanent"].asBool();
        }
        else
            _auxpermanent = false;
        
        aux.init(tag, backColor, _auxpermanent);
        int nButtons = json["Points"][i]["Buttons"].size();
        int yButton;
        int button_space;// = 100;
        int h;// = 300;
        
        h=(ofGetHeight()-100)/(nButtons+((nButtons-1.0)/3.0));
       // 50 == (ofGetHeight() - (nButtons * x + (nButtons -1 )*(x*1.0/3.0))/2);
         //   h = x.i;
        button_space = h/3;
        for(int j = 0; j < json["Points"][i]["Buttons"].size(); j++)
        {
            yButton = 50 + j*(h + button_space);
            button aux_button;
            aux_button.setColor(buttonColor, frontButtonColor);
            if(!json["Points"][i]["Buttons"][j]["Image"].asString().empty())
            {
                aux_button.init(j, json["Points"][i]["Buttons"][j]["Tag"].asString(), BUTTON_IMAGE,h, yButton);
                aux.setImage(json["Points"][i]["Buttons"][j]["Image"].asString());
            }
            else if(json["Points"][i]["Buttons"][j]["Video"].size() > 0 || !json["Points"][i]["Buttons"][j]["Video"].asString().empty())
            {
                aux_button.init(j, json["Points"][i]["Buttons"][j]["Tag"].asString(), BUTTON_VIDEO,h, yButton);
                if(json["Points"][i]["Buttons"][j]["Video"].size() > 0)
                {
                    for(int k = 0; k < json["Points"][i]["Buttons"][j]["Video"].size(); k++)
                    {
                        aux.setVideo(json["Points"][i]["Buttons"][j]["Video"][k].asString());
                    }
                }
                else
                {
                    aux.setVideo(json["Points"][i]["Buttons"][j]["Video"].asString());
                }
            }
            else if(!json["Points"][i]["Buttons"][j]["Action"].asString().empty())
            {
                aux_button.init(j, json["Points"][i]["Buttons"][j]["Tag"].asString(), BUTTON_ACTION,h, yButton);
                aux.setInteraction(json["Points"][i]["Buttons"][j]["Action"].asString());
            }
            aux.addButton(aux_button);
        }
        if(aux.getButtons().size() == 1)
        {
            aux.setSingle();
        }
        interface[tag] = aux;
    }
}

bool interface_manager::hasNewMsg()
{
    return _newMsg;
}

void interface_manager::resetNewMsg()
{
    _newMsg = false;
    message = "";
}
string interface_manager::getMsg()
{
    return message;
}

void interface_manager::update()
{
    if(activeInterface != "")
    {
	   if(interface[activeInterface].isActive())
       {     
            interface[activeInterface].update();
            if(interface[activeInterface].hasMsg())
            {
                _newMsg = true;
                message = interface[activeInterface].getMsg();
                interface[activeInterface].resetNewMsg();
            }
            if(interface[activeInterface].hasAction())
            {
                _newMsg = true;
                message = interface[activeInterface].getInteraction();
                interface[activeInterface].deactivateInterface();
                activeInterface = "";
            }
       }
	   else
       {
            _newMsg = true;
            //if(interface[activeInterface].isTimeout())
                message = "donothing";
            //else
            //    message = "done";

		  activeInterface = "";
        }
    }
}

void interface_manager::draw()
{
    ofBackground(backColor);
    if(activeInterface != "")
    {
        interface[activeInterface].draw();
    }
    else
    {
        back_main.draw(0,0,ofGetWidth(), ofGetHeight());
    }

    //ofPushStyle();
    //ofSetColor(255,255,0);
    //ofCircle(touchPoint, 5);
    //ofPopStyle();

}

void interface_manager::checkTouch(ofPoint _touchPoint)
{
    touchPoint = _touchPoint;
    touchPoint /= ofPoint(4000,4000);
    touchPoint.x *= ofGetWidth();
    touchPoint.y *= ofGetHeight();
    checkTouch(touchPoint.x, touchPoint.y);
}

void interface_manager::checkTouch(int mouseX, int mouseY)
{
    for (auto& i : interface)
    {
        if(i.second.isActive())
        {
            i.second.checkTouch(mouseX, mouseY);
            
        }
    }
}

void interface_manager::receiveCommand(string command)
{
    vector <string> auxs = ofSplitString(command, ":");
    if(auxs.size()> 0)
    {
        if(auxs[0] == "point")
        {
            if(activeInterface != "")
            {
                interface[activeInterface].deactivateInterface();
                activeInterface = auxs[1];
            }
            else
                activeInterface = auxs[1];

            interface[auxs[1]].activateInterface();


            /*if(auxs[1] == "undock")
            {
                _newMsg = true;
                message = "donothing";
            }*/
        }
    }
}

void interface_manager::changeInterface(int key)
{
    if(key=='1')
    {
        interface["0"].activateInterface();
        if(activeInterface != "")
        {
            interface[activeInterface].deactivateInterface();
            activeInterface = "0";
        }
        else
            activeInterface = "0";
    }
    else if(key=='2')
    {
        interface["1"].activateInterface();
        if(activeInterface != "")
        {
            interface[activeInterface].deactivateInterface();
            activeInterface = "1";
        }
        else
            activeInterface = "1";
    }
    else if(key =='3')
    {
        interface["2"].activateInterface();
        if(activeInterface != "")
        {
            interface[activeInterface].deactivateInterface();
            activeInterface = "2";
        }
        else
            activeInterface = "2";
    }
}
