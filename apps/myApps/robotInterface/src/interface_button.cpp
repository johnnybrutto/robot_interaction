//
//  interface_manager.cpp
//  robot_interface
//
//  Created by Joao Freire on 11/20/15.
//
//

#include "interface_button.h"

void button::init(int _iD, string _tag, button_type _type, int size, int y)
{
    width = ofGetWidth() - 100;
    height = size;
    iD = _iD;
    tag = _tag;
    type = _type;
    buttonArea.set(ofGetWidth()/2 - width/2, y, width, height);
    _clicked = false;
    verdana = new ofTrueTypeFont();
    verdana->load("verdana.ttf", 60, true, true);
}
    
void button::setColor(ofColor _back, ofColor _front)
{
    back = _back;
    front = _front;
}
bool button::isClicked(int x, int y)
{
    if(buttonArea.inside(x, y))
        _clicked = true;
    else
        _clicked = false;
        
    return _clicked;
}

button_type button::getType()
{
    return type;
}
    
void button::draw()
{
    ofPushStyle();
    ofSetColor(25,25,25,70);
    ofFill();
    ofPushMatrix();
    ofTranslate(15,15,0);
    ofRect(buttonArea);
    ofPopMatrix();
    ofPopStyle();
    ofPushStyle();
    ofSetColor(back);
    ofFill();
    ofRect(buttonArea);
    ofPopStyle();
    ofSetColor(front);
    verdana->drawString(tag, buttonArea.x + buttonArea.width/2 - verdana->stringWidth(tag)/2, buttonArea.y + buttonArea.height/2 + verdana->stringHeight(tag)/2);
}