//
//  interface_manager.cpp
//  robot_interface
//
//  Created by Joao Freire on 11/20/15.
//
//

#include "interface_point.h"

void interface_point::init(string _tag, ofColor backColor, bool _permanent)
{
    ofTrueTypeFont::setGlobalDpi(72);
    tag = _tag;
    _interfaceActive = false;
    _hasButtonActive = false;
    back.loadImage("images/heart.png");
    activeButton = -1;
    activeVideo = 0;
    _isSingle = false;
    _sendAction = false;
    _timeout = false;
    background = backColor;
    _isPermanent = _permanent;
    _hasMsg = false;
    _waiting = false;
}

void interface_point::addButton(button aux)
{
    buttons.push_back(aux);
}

void interface_point::setImage(string path)
{
    image.loadImage(path);
}

void interface_point::setVideo(string path)
{
    ofVideoPlayer video;
    video.loadMovie(path);
	video.setLoopState(OF_LOOP_NONE);
    video.play();
	video.setFrame(0);
    video.update();
    video.setPaused(true);
    videos.push_back(video);
}

void interface_point::setSingle()
{
    _isSingle = true;
    activeButton = 0;
}

void interface_point::setInteraction(string _interaction)
{
    interaction = _interaction;
}

string interface_point::getInteraction()
{
    return interaction;
}

vector <button> interface_point::getButtons()
{
    return buttons;
}
    
bool interface_point::hasAction()
{
    return _sendAction;
}

bool interface_point::isTimeout()
{
    return _timeout;
}

void interface_point::activateInterface()
{
    _interfaceActive = true;
    _timeout = false;
    active_timer = ofGetElapsedTimef();
    if(videos.size() > 0)
    {
        activeVideo = ofRandom(videos.size());
        videos[activeVideo].setVolume(0);
        videos[activeVideo].play();
        videos[activeVideo].setFrame(20);
        videos[activeVideo].update();
        if(!_isSingle)
            videos[activeVideo].setPaused(true);
        videos[activeVideo].setVolume(1);
    }
}

void interface_point::deactivateInterface()
{
    _interfaceActive = false;
    _hasButtonActive = false;
    _sendAction = false;
    _waiting = false;
    if(videos.size() > 0)
    {
        videos[activeVideo].setFrame(0);
        videos[activeVideo].play();
        videos[activeVideo].setPaused(true);
    }
}

bool interface_point::isActive()
{
    return _interfaceActive;
}

bool interface_point::isPermanent()
{
    return _isPermanent;
}

bool interface_point::hasMsg()
{
    return _hasMsg;
}

string interface_point::getMsg()
{
    return msg;
}
void interface_point::resetNewMsg()
{
    _hasMsg = false;
    msg = "";
}

void interface_point::sendThis(string _msg)
{
    _hasMsg = true;
    msg = _msg;
}

int interface_point::checkTouch(int mouseX, int mouseY)
{
    if(!_hasButtonActive)
    {
        for(int i = 0; i < buttons.size(); i++)
        {
            if(buttons[i].isClicked(mouseX, mouseY))
            {
                _hasButtonActive = true;
                activeButton = i;

                if(buttons[i].getType() == BUTTON_VIDEO)
                {
                    videos[activeVideo].play();
                }
                else if(buttons[i].getType() == BUTTON_ACTION)
                {
                    _sendAction = true;
                }
                return i;
            }
        }
    }
    return -1;
}

void interface_point::update()
{
    if(_interfaceActive)
    {
        if(_hasButtonActive || _isSingle)
        {
            if(isPermanent())
            {
                 if(ofGetElapsedTimef() - active_timer > 1 && !_waiting)
                 {
                    sendThis("donothing");
                    _waiting = true;
                 }
            }

            if(buttons[activeButton].getType() == BUTTON_VIDEO)
            {
                videos[activeVideo].update();
                if(videos[activeVideo].getIsMovieDone())
                {
                    videos[activeVideo].setFrame(0);
                    videos[activeVideo].play();
                    if(!isPermanent())
                    {
                        videos[activeVideo].setPaused(true);
				        deactivateInterface();
                    }   
                }
            }
            else 
            {
                if(ofGetElapsedTimef() - active_timer > 20 && !isPermanent())
                {
                    deactivateInterface();
                }
            }
        }
        else
        {
            if(ofGetElapsedTimef() - active_timer > 10)
            { 
                deactivateInterface();
                _timeout = true;
            }
        }
    }
}

void interface_point::draw()
{
    if(_interfaceActive)
    {
        if(!_hasButtonActive && !_isSingle)
        {
            ofSetColor(255,255,255);            
            for(int i = 0; i < buttons.size(); i++)
            {
                buttons[i].draw();
            }
        }
        else
        {
            if(buttons[activeButton].getType() == BUTTON_IMAGE)
                image.draw(0,0,ofGetWidth(), ofGetHeight());
            else if(buttons[activeButton].getType() == BUTTON_VIDEO)
            {
                videos[activeVideo].draw(0,0,ofGetWidth(), ofGetHeight());
            }
            else
            {
                ofSetColor(255,255,255);
                ofPushMatrix();
                back.setAnchorPercent(0.5,0.5);
                ofTranslate(ofGetWidth()*0.5, ofGetHeight()*0.5 );
                ofScale(1.05+ 0.05*sin(2*ofGetElapsedTimef()), 1.05+ 0.05*sin(2*ofGetElapsedTimef()));
                back.draw(0,0);
                ofPopMatrix();
            }
        }
    }
}
