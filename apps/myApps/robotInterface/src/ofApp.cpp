#include "ofApp.h"

ofApp::ofApp(ofxArgs* args){
    this->args = args;
}
//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetWindowTitle("robot interaction");
    //this->args->printArgs();
    //this->args->printOpts();
    
    // Read application settings...
    // initialize default option values in case they are not passed
    this->option1 = this->option2 = "";
    this->flag1 = false;
    
    if(this->args->contains("path"))
    {
        this->option1 = this->args->getString("path");
    }
 
    if(this->args->contains("opt2"))
    {
        this->option2 = this->args->getString("opt2");
    }
    this->flag1 = this->args->contains("-f1");
    ofSetDataPathRoot(this->option1);
    gui_set = false;
    interface_set = false;
    createGui();
    COM.create(interaction_topic, touch_topic); 
    //manager.create( json_path, buttonColor, frontColor, backColor);
    _hideGui = true;
    
}

void ofApp::createGui()
{
    gui.setup(); 
    gui.add(mouseInput.setup("use Mouse", false));
    gui.add(buttonColor.setup("button Color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));
    gui.add(frontColor.setup("tag Color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));
    gui.add(backColor.setup("back Color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));

    gui.add(interaction_topic.setup("Inter", "/idmind_interface"));
    gui.add(touch_topic.setup("Touch", "/idmind_touch"));
    //gui.add(root_path.setup("Root", "/home/freire/Dropbox/media/"));
    gui.add(json_path.setup("JSON", "prog_points.json"));

    gui.loadFromFile("gui_settings.xml");
    cout<<"SET GUI"<<endl;
    gui_set = true;

}
//--------------------------------------------------------------
void ofApp::update()
{
    if(gui_set && ofGetWidth() == 720 && !interface_set)
    {            
        cout<<"CREATE MANAGER"<<endl;
        manager.create( json_path, buttonColor, frontColor, backColor); 
        COM.send("connect");
        interface_set = true;   
    }
	
    if(interface_set)
    {
        if(COM.hasNewMsg())
        {
            manager.receiveCommand(COM.getLastMsg());
        }

        if(COM.hasNewTouch())
        {
            manager.checkTouch(COM.getTouch());
        }

        manager.update();
        if(manager.hasNewMsg())
        {
            COM.send(manager.getMsg());

            manager.resetNewMsg();
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetColor(255,255,255);
    manager.draw();
    ofPushStyle();
    ofSetColor(255,255,255);
    if(!_hideGui)
        gui.draw();
    ofPopStyle();

    //verdana.drawString("Font Example - use keyboard to type", 30, 35);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    manager.changeInterface(key);
    if (key == 'h')
    {
        _hideGui = !_hideGui;
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    manager.checkTouch(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
cout<<"Resize with: "<<w<<"  ;  "<<h<<endl;
    if(w < h  )
    {
		cout<<"Trying to bring back focus"<<endl;
        ofSetWindowShape(720, 1280);
	}
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::exit()
{
    gui.saveToFile("gui_settings.xml");
}
