//
//  communication_manager.cpp
//  robot_interface
//
//  Created by Joao Freire on 11/26/15.
//
//

#include "communication_manager.h"

void communication_manager::create(string _interaction_topic, string _touch_topic)
{
    ofxLibwebsockets::ClientOptions options = ofxLibwebsockets::defaultClientOptions();
    options.host = "ws://192.168.2.24";
    
    options.ka_time     = 1;
    options.ka_probes   = 1;
    options.ka_interval = 1;
    // 4 - connect
    client.connect("localhost",9090);
    
    ofSetLogLevel(OF_LOG_ERROR);
    interaction_topic = _interaction_topic;
    touch_topic = _touch_topic;

    client.addListener(this);
    string send =  " {\"op\":\"advertise\",\"topic\":\"" + interaction_topic + "/\",\"type\":\"std_msgs/String\"}";
	client.send(send);
    send = " {\"op\":\"subscribe\",\"topic\":\"" + interaction_topic+ "/\"}";
	client.send(send);
    send = " {\"op\":\"subscribe\",\"topic\":\"" + touch_topic + "/\"}";
    client.send(send);
	_newTouch = false;
    _newMsg = false;
}

void communication_manager::send(string msg)
{
    string send;
    send = "{ \"op\": \"publish\", \"topic\":\""  + interaction_topic+"/\",\"msg\": {\"data\":\"action:" + msg+ "\"}}";
   /* if(msg == "done")
    {    
        client.send(send);
        ofSleepMillis(300);
        send = "{ \"op\": \"publish\", \"topic\":\"" + interaction_topic+"/\",\"msg\": {\"data\":\"action:donothing\"}}";
        client.send(send);
    }
    else*/
    client.send(send);
}

bool communication_manager::hasNewMsg()
{
    return _newMsg;
}

string communication_manager::getLastMsg()
{
    _newMsg = false;
    return lastMsg;
}

bool communication_manager::hasNewTouch()
{
    return _newTouch;
}

ofPoint communication_manager::getTouch()
{
    _newTouch = false;
    return touchPoint;
}

void communication_manager::decodeMsg(string msg)
{
	msgParser.parse(msg);
	string topic = msgParser["topic"].asString();

    if(topic.find(interaction_topic) == 0)
	{
        _newMsg = true;
        lastMsg = msgParser["msg"]["data"].asString();
	}
    else if(topic.find(touch_topic) == 0)
    {
        _newTouch = true;
        
        touchPoint.x = msgParser["msg"]["x"].asInt();
        touchPoint.y = msgParser["msg"]["y"].asInt();
    }
}

void communication_manager::onConnect( ofxLibwebsockets::Event& args )
{
    cout<<"COM connected"<<endl;
}

void communication_manager::onOpen( ofxLibwebsockets::Event& args )
{
        
}

void communication_manager::onClose( ofxLibwebsockets::Event& args )
{
}

void communication_manager::onIdle( ofxLibwebsockets::Event& args )
{
}

void communication_manager::onMessage( ofxLibwebsockets::Event& args )
{
	decodeMsg(args.message);
}

void communication_manager::onBroadcast( ofxLibwebsockets::Event& args )
{
}
  
