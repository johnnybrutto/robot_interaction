//
//  interface_point.h
//  robot_interface
//
//  Created by Joao Freire on 11/20/15.
//
//

#ifndef interface_point_h
#define interface_point_h

#include <stdio.h>
#pragma once

#include "ofMain.h"
#include "ofxJSON.h"
#include "interface_button.h"


class interface_point
{
public:
    void init(string _tag, ofColor backColor, bool _permanent);
    void addButton(button aux);
    void setImage(string path);
    void setVideo(string path);
    void setInteraction(string interaction);
    string getInteraction();
    void activateInterface();
    void deactivateInterface();
    bool isActive();
    int checkTouch(int mouseX, int mouseY);
    void update();
    void draw();
    void setSingle();
    vector <button> getButtons();
    bool hasAction();
    bool isTimeout();
    bool isPermanent();
    void sendThis(string _msg);
    bool hasMsg();
    string getMsg();
    void resetNewMsg();

private:
    vector <ofVideoPlayer> videos;
    ofImage image, video_image;
    string tag;
    string interaction;
    bool _interfaceActive;
    bool _hasButtonActive;
    ofImage back;
    vector<button> buttons;
    int activeButton;
    int active_timer;
    int activeVideo;
    bool _isSingle;  
    bool _sendAction; 
    bool _timeout; 
    ofColor background;
    bool _isPermanent;
    bool _hasMsg;
    string msg;
    bool _waiting;

};
#endif /* interface_manager_h */
